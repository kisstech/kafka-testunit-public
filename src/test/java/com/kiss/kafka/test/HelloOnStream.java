package com.kiss.kafka.test;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello KStream!
 * kafka-topics --zookeeper localhost:2181 --create --topic stream-topic-in --partitions 10 --replication-factor 1
 * kafka-topics --zookeeper localhost:2181 --create --topic stream-topic-out --partitions 10 --replication-factor 1
 * <p>
 * kafka-console-producer --broker-list localhost:9092  --topic stream-topic-in
 * kafka-console-consumer --bootstrap-server localhost:9092 --topic stream-topic-out
 */
public class HelloOnStream {

    private Logger logger = LoggerFactory.getLogger(getClass());

    final static String TOPIC_IN = "stream-topic-in";
    final static String TOPIC_OUT = "stream-topic-out";

    final StreamsBuilder builder;

    public HelloOnStream(StreamsBuilder builder) {
        this.builder = builder;
        // Building stream topology
        KStream<String, String> source = builder.stream(TOPIC_IN, Consumed.with(Serdes.String(), Serdes.String()));
        source
                .filter((key, value) -> !"".equals(value))
                .peek((key, value) -> logger.info("__key={}, value={}", key, value))
                .mapValues(value -> value.toUpperCase())
                .to(TOPIC_OUT, Produced.with(Serdes.String(), Serdes.String()));
    }
}

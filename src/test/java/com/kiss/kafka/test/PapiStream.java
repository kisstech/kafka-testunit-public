package com.kiss.kafka.test;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * Hello KStream!
 * kafka-topics --zookeeper localhost:2181 --create --topic stream-topic-in --partitions 10 --replication-factor 1
 * kafka-topics --zookeeper localhost:2181 --create --topic stream-topic-out --partitions 10 --replication-factor 1
 * <p>
 * kafka-console-producer --broker-list localhost:9092  --topic stream-topic-in
 * kafka-console-consumer --bootstrap-server localhost:9092 --topic stream-topic-out
 */
public class PapiStream {

    public static final String STORE_ACCUMULATOR = "Accumulator";
    private Logger logger = LoggerFactory.getLogger(getClass());

    final static String TOPIC_IN = "stream-topic-in";
    final static String TOPIC_OUT = "stream-topic-out";
    private KafkaStreams streams;

    private final String bootstrapServers;

    public PapiStream(String bootstrapServers) {
        this.bootstrapServers = bootstrapServers;
    }

    public void start() {
        Properties config = new Properties();
        // Connection
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, PapiStream.class.getSimpleName());
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

        // Serialization
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        // Building stream topology
//Serializers for types used in the processors

        StringDeserializer stringDeserializer = new StringDeserializer();
        StringSerializer stringSerializer = new StringSerializer();

        StoreBuilder<KeyValueStore<String, String>> accumulatorStoreBuilder = Stores.keyValueStoreBuilder(
                Stores.persistentKeyValueStore(STORE_ACCUMULATOR),
                Serdes.String(),
                Serdes.String())
                .withLoggingDisabled(); // disable backing up the store to a changelog topic

        Topology topology = new Topology();
        topology.addSource("SOURCE", stringDeserializer, stringDeserializer, TOPIC_IN)
                .addProcessor("PROCESS", () -> new AccumulatorProcessor(), "SOURCE")
                .addStateStore(accumulatorStoreBuilder, "PROCESS")
                .addSink("SINK", TOPIC_OUT, stringSerializer, stringSerializer, "PROCESS");

//Use the topologyBuilder and streamingConfig to start the kafka streams process
        streams = new KafkaStreams(topology, config);
        streams.start();
    }

    public void destroy() {
        streams.close();
    }
}

class AccumulatorProcessor implements Processor<String, String> {

    private ProcessorContext context;
    private KeyValueStore<String, String> kvStore;

    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        // keep the processor context locally because we need it in punctuate() and commit()
        this.context = context;

        // retrieve the key-value store named "Counts"
        kvStore = (KeyValueStore) context.getStateStore("Accumulator");

        // schedule a punctuate() method every 1000 milliseconds based on clock time
        this.context.schedule(1000, PunctuationType.STREAM_TIME, (timestamp) -> {
            // Send all
            KeyValueIterator<String, String> iter = this.kvStore.all();
            while (iter.hasNext()) {
                KeyValue<String, String> entry = iter.next();
                context.forward(entry.key, entry.value);
                kvStore.delete(entry.key);
            }
            iter.close();

            // commit the current processing progress
            context.commit();
        });
    }

    @Override
    public void process(String key, String value) {
        String storeValue = kvStore.get(key);
        if (storeValue == null) {
            storeValue = value;
        } else {
            storeValue = storeValue + " | " + value;
        }
        kvStore.put(key, storeValue);
    }

    @Override
    public void close() {
        // close any resources managed by this processor
        // Note: Do not close any StateStores as these are managed by the library
    }

}
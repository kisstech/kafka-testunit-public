package com.kiss.kafka.test;


import com.kiss.formation.kafka.avro.Address;
import com.kiss.formation.kafka.avro.Company;
import com.kiss.kafka.test.fmk.SchemaRegistryMock;
import com.kiss.kafka.test.fmk.TestInput;
import com.kiss.kafka.test.fmk.TestTopology;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.scala.Serdes;
import org.junit.Test;

import java.util.Collections;
import java.util.Map;
import java.util.Properties;

import static com.kiss.kafka.test.HelloOnStreamWithAvro.TOPIC_IN;
import static com.kiss.kafka.test.HelloOnStreamWithAvro.TOPIC_OUT;

public class HelloOnStreamWithAvroUnitTest {

    @Test
    public void testKStream() {
        SchemaRegistryMock schemaRegistryMock = new SchemaRegistryMock();
        schemaRegistryMock.start();

        // using DSL
        StreamsBuilder builder = new StreamsBuilder();
        new HelloOnStreamWithAvro(builder, schemaRegistryMock.getUrl());

        // Create topology
        Topology topology = builder.build();

        // setup test driver
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        TestTopology<String, String> testTopology = new TestTopology(topology, props, schemaRegistryMock);
        testTopology.start();

        final Map<String, String> serdeConfig = Collections.singletonMap(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG,
                schemaRegistryMock.getUrl());
        SpecificAvroSerde<Company> companySpecificAvroSerde = new SpecificAvroSerde<>();
        companySpecificAvroSerde.configure(serdeConfig, false);

        Serde<String> stringSerde = Serdes.String();
        TestInput<String, Company> input = testTopology.input(TOPIC_IN)
                .withValueSerde(companySpecificAvroSerde)
                .withKeySerde(stringSerde);

        Company companyFake = Company.newBuilder()
                .setName("FakeOne")
                .setFirstName("Bob")
                .setLastName("Jones")
                .setYearOfCreation(2017)
                .setAddress(
                        Address.newBuilder()
                                .setAddress1("Address1")
                                .setCity("Paris")
                                .setZipCode(75000)
                                .build()).build();

        input.add(companyFake);

        testTopology.streamOutput(TOPIC_OUT)
                .withTypes(String.class, Company.class) //
                .withKeySerde(stringSerde)
                .withValueSerde(companySpecificAvroSerde)
                .expectNextRecord()
                .hasValue(value -> {return null;})
                .expectNoMoreRecord();
    }
}

package com.kiss.kafka.test;


import com.kiss.kafka.test.fmk.TestInput;
import com.kiss.kafka.test.fmk.TestOutput;
import com.kiss.kafka.test.fmk.TestTopology;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.scala.Serdes;
import org.junit.Test;

import java.util.Properties;

import static com.kiss.kafka.test.HelloOnStream.TOPIC_IN;
import static com.kiss.kafka.test.HelloOnStream.TOPIC_OUT;

public class HelloOnStreamUnitTest {

    @Test
    public void testKStream() {
        // using DSL
        StreamsBuilder builder = new StreamsBuilder();
        HelloOnStream helloOnStream = new HelloOnStream(builder);

        // Create topology
        Topology topology = builder.build();

        // setup test driver
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        TopologyTestDriver testDriver = new TopologyTestDriver(topology, props);
        TestTopology<String, String> testTopology = new TestTopology(topology, props, null);
        testTopology.start();

        Serde<String> stringSerde = Serdes.String();
        TestInput<String, String> input = testTopology.input(TOPIC_IN)
                .withValueSerde(stringSerde)
                .withKeySerde(stringSerde);

        input.add("a");
        input.add("b");
        input.add("c");

        testTopology.streamOutput(TOPIC_OUT)
                .withTypes(String.class, String.class) //
                .withKeySerde(stringSerde)
                .withValueSerde(stringSerde)
                .expectNextRecord()
                .hasValue(value -> {return null;})
                .expectNextRecord()
                .hasValue(value -> {return null;})
                .expectNextRecord()
                .hasValue(value -> {return null;})
                .expectNoMoreRecord();
    }
}

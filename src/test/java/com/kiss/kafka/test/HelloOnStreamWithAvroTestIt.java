package com.kiss.kafka.test;


import com.kiss.formation.kafka.avro.Address;
import com.kiss.formation.kafka.avro.Company;
import io.confluent.examples.streams.kafka.EmbeddedSingleNodeKafkaCluster;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.streams.serdes.avro.GenericAvroDeserializer;
import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroDeserializer;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.junit.*;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import static com.kiss.kafka.test.HelloOnStreamWithAvro.TOPIC_IN;
import static com.kiss.kafka.test.HelloOnStreamWithAvro.TOPIC_OUT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HelloOnStreamWithAvroTestIt {

    @ClassRule
    public static final EmbeddedSingleNodeKafkaCluster CLUSTER = new EmbeddedSingleNodeKafkaCluster();
    private Properties config;
    private HelloOnStreamWithAvro helloOnStream;
    private String bootstrapServers;
    private String registryUrl;
    private Topology topology;
    private KafkaStreams streams;

    @BeforeClass
    public static void startKafkaCluster() throws Exception {
        CLUSTER.createTopic(TOPIC_IN);
        CLUSTER.createTopic(TOPIC_OUT);
    }

    @Before
    public void setup() {
        bootstrapServers = CLUSTER.bootstrapServers();
        registryUrl = CLUSTER.schemaRegistryUrl();

        StreamsBuilder builder = new StreamsBuilder();
        helloOnStream = new HelloOnStreamWithAvro(builder, registryUrl);

        config = new Properties();
        // Connection
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, HelloOnStreamWithAvro.class.getSimpleName());
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        config.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, registryUrl);

        // Serialization
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, GenericAvroSerde.class);

        topology = builder.build();
        streams = new KafkaStreams(topology, config);
        streams.start();
    }

    @After
    public void destroy() {
        streams.close();
    }

    @Test
    public void testKStream() throws ExecutionException, InterruptedException {
        Company companyFake = Company.newBuilder()
                .setName("FakeOne")
                .setFirstName("Bob")
                .setLastName("Jones")
                .setYearOfCreation(2017)
                .setAddress(
                        Address.newBuilder()
                                .setAddress1("Address1")
                                .setCity("Paris")
                                .setZipCode(75000)
                                .build()).build();

        // Could use convenience methods to produce and consume messages
        Properties producerConfig = new Properties();
        producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        producerConfig.put(ProducerConfig.ACKS_CONFIG, "all");
        producerConfig.put(ProducerConfig.RETRIES_CONFIG, 0);
        producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        producerConfig.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, registryUrl);
        List<Company> inputValues = Arrays.asList(companyFake);
        IntegrationTestUtils.produceValuesSynchronously(TOPIC_IN, inputValues, producerConfig);

        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "avro-standard-consumer");
        consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, SpecificAvroDeserializer.class);
        consumerConfig.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, registryUrl);
        List<Company> result = IntegrationTestUtils.waitUntilMinValuesRecordsReceived(consumerConfig,
                TOPIC_OUT, inputValues.size());

        assertTrue(result.contains(companyFake));
    }

    @Test
    public void testKStreamGenericAvro() throws ExecutionException, InterruptedException {
        Company companyFake = Company.newBuilder()
                .setName("FakeOne")
                .setFirstName("Bob")
                .setLastName("Jones")
                .setYearOfCreation(2017)
                .setAddress(
                        Address.newBuilder()
                                .setAddress1("Address1")
                                .setCity("Paris")
                                .setZipCode(75000)
                                .build()).build();

        // Could use convenience methods to produce and consume messages
        Properties producerConfig = new Properties();
        producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
        producerConfig.put(ProducerConfig.ACKS_CONFIG, "all");
        producerConfig.put(ProducerConfig.RETRIES_CONFIG, 0);
        producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        producerConfig.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, CLUSTER.schemaRegistryUrl());
        List<Company> inputValues = Arrays.asList(companyFake);
        IntegrationTestUtils.produceValuesSynchronously(TOPIC_IN, inputValues, producerConfig);

        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "avro-standard-consumer");
        consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, GenericAvroDeserializer.class);
        consumerConfig.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, CLUSTER.schemaRegistryUrl());
        List<GenericRecord> result = IntegrationTestUtils.waitUntilMinValuesRecordsReceived(consumerConfig,
                TOPIC_OUT, inputValues.size());

        assertEquals("Bob", result.get(0).get("firstName").toString());
    }
}

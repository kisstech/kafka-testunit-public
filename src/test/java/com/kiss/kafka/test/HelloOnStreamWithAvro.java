package com.kiss.kafka.test;

import com.kiss.formation.kafka.avro.Company;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;

/**
 * Hello KStream With AVRO!
 * kafka-topics --zookeeper localhost:2181 --create --topic stream-topic-in-avro --partitions 10 --replication-factor 1
 * kafka-topics --zookeeper localhost:2181 --create --topic stream-topic-out-avro --partitions 10 --replication-factor 1
 * <p>
 * kafka-console-producer --broker-list localhost:9092  --topic stream-topic-in-avro
 * kafka-console-consumer --bootstrap-server localhost:9092 --topic stream-topic-out-avro
 */
public class HelloOnStreamWithAvro {

    private Logger logger = LoggerFactory.getLogger(getClass());

    final static String TOPIC_IN = "stream-topic-in-avro";
    final static String TOPIC_OUT = "stream-topic-out-avro";
    private KafkaStreams streams;

    final StreamsBuilder builder;
    private final String registryUrl;

    public HelloOnStreamWithAvro(StreamsBuilder builder, String registryUrl) {
        this.builder = builder;
        this.registryUrl = registryUrl;


        // When you want to override serdes explicitly/selectively
        final Map<String, String> serdeConfig = Collections.singletonMap(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG,
                this.registryUrl);
        final SpecificAvroSerde<Company> companyAvroSerde = new SpecificAvroSerde<>();
        companyAvroSerde.configure(serdeConfig, false);

        // Building stream topology

        KStream<String, Company> source = builder.stream(TOPIC_IN, Consumed.with(Serdes.String(), companyAvroSerde));
        source
                .filter((key, value) -> !"".equals(value))
                .peek((key, value) -> logger.info("__key={}, value={}", key, value))
                .mapValues(value -> value)
                .to(TOPIC_OUT, Produced.with(Serdes.String(), companyAvroSerde));
    }
}

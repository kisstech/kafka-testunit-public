package com.kiss.kafka.test;


import io.confluent.examples.streams.kafka.EmbeddedSingleNodeKafkaCluster;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KeyValue;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import static com.kiss.kafka.test.PapiStream.TOPIC_IN;
import static com.kiss.kafka.test.PapiStream.TOPIC_OUT;
import static org.junit.Assert.assertTrue;

@Ignore
public class PapiStreamTest {

    @ClassRule
    public static final EmbeddedSingleNodeKafkaCluster CLUSTER = new EmbeddedSingleNodeKafkaCluster();

    @BeforeClass
    public static void startKafkaCluster() throws Exception {
        CLUSTER.createTopic(TOPIC_IN);
        CLUSTER.createTopic(TOPIC_OUT);
    }

    @Test
    public void testKStream() throws ExecutionException, InterruptedException {
        PapiStream stream = new PapiStream(CLUSTER.bootstrapServers());
        stream.start();

        // Could use convenience methods to produce and consume messages
        Properties producerConfig = new Properties();
        producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
        producerConfig.put(ProducerConfig.ACKS_CONFIG, "all");
        producerConfig.put(ProducerConfig.RETRIES_CONFIG, 0);
        producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        producerConfig.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, CLUSTER.schemaRegistryUrl());
        List<KeyValue<String, String>> inputs1 = Arrays.asList(
                new KeyValue<>("1", "a"),
                new KeyValue<>("2", "a"),
                new KeyValue<>("3", "a")
        );
        IntegrationTestUtils.produceKeyValuesSynchronously(TOPIC_IN, inputs1, producerConfig);
        IntegrationTestUtils.produceKeyValuesSynchronously(TOPIC_IN, inputs1, producerConfig);

        Thread.sleep(5000);

        List<KeyValue<String, String>> inputs2 = Arrays.asList(
                new KeyValue<>("1", "e"),
                new KeyValue<>("1", "f"),
                new KeyValue<>("1", "g"),
                new KeyValue<>("2", "b")
        );
        IntegrationTestUtils.produceKeyValuesSynchronously(TOPIC_IN, inputs2, producerConfig);

        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "generic-standard-consumer");
        consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerConfig.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, CLUSTER.schemaRegistryUrl());
        List<KeyValue<String, String>> keyValues = IntegrationTestUtils.waitUntilMinKeyValueRecordsReceived(consumerConfig,
                TOPIC_OUT, 4);
        stream.destroy();

    }
}

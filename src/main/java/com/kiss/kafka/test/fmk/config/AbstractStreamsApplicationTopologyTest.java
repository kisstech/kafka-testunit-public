package com.kiss.kafka.test.fmk.config;

import com.kiss.kafka.test.fmk.SchemaRegistryMock;
import com.kiss.kafka.test.fmk.TestTopology;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.util.Properties;

@Slf4j
public abstract class AbstractStreamsApplicationTopologyTest<KEY, VALUE>  {
	@Autowired
    protected SchemaRegistryMock schemaRegistryMock;
    @Autowired
    protected Properties kafkaConfiguration;
    @Autowired
    protected StreamsBuilderFactoryBean defaultKafkaStreamsBuilder;
    @Autowired
    protected StreamsBuilder streamsBuilder;
    
    protected TestTopology<KEY, VALUE> testTopologyRule;

    protected Topology topology;

    @Before
    public void start() {
        if(testTopologyRule==null) {
        	topology=streamsBuilder.build();
        	log.trace(topology.describe().toString());
            testTopologyRule = new TestTopology<>(topology, kafkaConfiguration, schemaRegistryMock);
        }
        this.testTopologyRule.start();
        ((StreamsBuilderFactoryBeanDrivers)defaultKafkaStreamsBuilder).applyTestDriver(topology, this.testTopologyRule.getTestDriver());
    }

    @After
    public void stop() {
        topology = null;
        if(System.getProperty("os.name").toLowerCase().contains("windows")) {
            try {
                this.testTopologyRule.getTestDriver().close();
            } catch (Exception e) {
                String stateDir = (String)this.testTopologyRule.getProperties().get(StreamsConfig.STATE_DIR_CONFIG);
                FileSystemUtils.deleteRecursively(new File(stateDir));
            }
        } else {
            this.testTopologyRule.stop();
        }
    }
}

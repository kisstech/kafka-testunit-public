package com.kiss.kafka.test.fmk.config;

import java.util.Map;
import java.util.Properties;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.state.QueryableStoreType;

public class KafkaStreamsDrivers extends KafkaStreams{
	private TopologyTestDriver testDriver;
	
	public KafkaStreamsDrivers(Topology topology, Properties props) {
		super(topology, props);
	}
	
	public <T> T store(final String storeName, final QueryableStoreType<T> queryableStoreType) {
		 Map<String, StateStore> values=testDriver.getAllStateStores();
		return (T) testDriver.getKeyValueStore(storeName);
	}

	@Override
	public <T> T store(StoreQueryParameters<T> storeQueryParameters) {
		Map<String, StateStore> values=testDriver.getAllStateStores();
		return (T) testDriver.getKeyValueStore(storeQueryParameters.storeName());
	}

	public void setTopologyTestDriver(TopologyTestDriver testDriver) {
		this.testDriver=testDriver;
	}
}

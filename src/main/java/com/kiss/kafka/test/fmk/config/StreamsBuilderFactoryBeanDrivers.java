package com.kiss.kafka.test.fmk.config;

import java.util.Properties;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;


public class StreamsBuilderFactoryBeanDrivers extends StreamsBuilderFactoryBean {
	private final Properties properties;
	private KafkaStreamsDrivers kafkaStreamsDrivers;
	private StreamsBuilder builder;
	
	public StreamsBuilderFactoryBeanDrivers(StreamsBuilder streamsBuilder,Properties properties) {
		this.builder=streamsBuilder;
		this.properties = properties;
	}

	protected StreamsBuilder createInstance() {
		return this.builder;
	}

	@Override
	public KafkaStreams getKafkaStreams() {
		return kafkaStreamsDrivers;
	}

	@Override
	public synchronized void start() {
	}
	
	public void applyTestDriver(Topology topology, TopologyTestDriver testDriver) {
		this.kafkaStreamsDrivers=new KafkaStreamsDrivers(topology, properties);
		kafkaStreamsDrivers.setTopologyTestDriver(testDriver);
	}

}

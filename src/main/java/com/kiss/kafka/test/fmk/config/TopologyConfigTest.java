package com.kiss.kafka.test.fmk.config;

import java.io.IOException;
import java.util.Properties;

import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;

import com.kiss.kafka.test.fmk.SchemaRegistryMock;

import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;

@Configuration
public class TopologyConfigTest {
	
	  @Bean
      @ConfigurationProperties(prefix = "kafka")
      public Properties kafkaConfiguration(String schemaRegistryUrl) {
          final String brokers = "localhost:9092";
          Properties props = new Properties();
          props.put("bootstrap.servers", brokers);
          props.put("schema.registry.url", schemaRegistryUrl);
          props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, SpecificAvroSerde.class.getName());
          props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SpecificAvroSerde.class.getName());
          return props;
      }

      @Bean
      public SchemaRegistryMock schemaRegistryMock() throws IOException, RestClientException {
          SchemaRegistryMock schemaRegistry = new SchemaRegistryMock();
          schemaRegistry.start();
       /*   for (Topics topic : Topics.values()) {
              if (topic.getSchemaKey() != null) {
                  schemaRegistry.registerKeySchema(topic.getTopic(), topic.getSchemaKey());
              }
              schemaRegistry.registerValueSchema(topic.getTopic(), topic.getSchemaValue());
          }*/
          return schemaRegistry;
      }

      @Bean
      public String schemaRegistryUrl() throws IOException, RestClientException {
          return schemaRegistryMock().getUrl();
      }

      @Bean
      @Primary
      public StreamsBuilder streamsBuilder() {
          return new StreamsBuilder();
      }
      
      @Bean
      public StreamsBuilderFactoryBean defaultKafkaStreamsBuilder() throws IOException, RestClientException {
      	StreamsBuilderFactoryBean streamBuilderFactory=new StreamsBuilderFactoryBeanDrivers(streamsBuilder(),kafkaConfiguration(schemaRegistryUrl()));
      	return streamBuilderFactory;
      }
}
